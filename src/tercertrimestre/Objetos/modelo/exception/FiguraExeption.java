package tercertrimestre.modelo.exception;

public class FiguraExeption extends Exception {
	
	private static final long serialVersionUID = 1L;

	public FiguraExeption(String message) {
		super(message);
	}
}
