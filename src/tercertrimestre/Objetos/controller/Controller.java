package tercertrimestre.controller;

import tercertrimestre.modelo.Figura;
import tercertrimestre.modelo.exception.FiguraExeption;

public interface Controller {
	public void addHandler(Figura fig) throws FiguraExeption;
	public void leerHandler(Figura fig) throws FiguraExeption;
	public void modifyHandler(Figura fig) throws FiguraExeption;
	public void remomveHandler(Figura fig) throws FiguraExeption;
}
