package tercertrimestre.controller;

import tercertrimestre.controller.composite.ValidatorComposite;
import tercertrimestre.modelo.Figura;
import tercertrimestre.modelo.exception.FiguraExeption;

public class FiguraC implements Controller {

	public FiguraC() {
	}

	@Override
	public void addHandler(Figura fig) throws FiguraExeption {
		String strErrores=ValidatorComposite.getErrores(fig);
		if(!strErrores.isEmpty())
			throw new FiguraExeption(strErrores);
	

	}

	@Override
	public void leerHandler(Figura fig) throws FiguraExeption {
		

	}

	@Override
	public void modifyHandler(Figura fig) throws FiguraExeption {
		

	}

	@Override
	public void remomveHandler(Figura fig) throws FiguraExeption {
		

	}

}
