package tercertrimestre.controller.composite;

import tercertrimestre.modelo.Rectangulo;

public class RecBaseNegativa extends ValidatorComposite {

	public RecBaseNegativa() {
	
	}

	@Override
	public boolean isMe() {
		return figura instanceof Rectangulo;
	}

	@Override
	public boolean validar() {
		Rectangulo rec = (Rectangulo)figura;
		return rec.getBase()<=0;
	
	}

	@Override
	public String getError() {
		// hacer un downcast
		return "La base debe ser mayor que 0 (cero)";
	}

}
