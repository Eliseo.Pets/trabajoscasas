package tercertrimestre.controller.composite;

import tercertrimestre.modelo.Circulo;

public class CircRadioNegativo extends ValidatorComposite {

	public CircRadioNegativo() {
	
	}

	@Override
	public boolean isMe() {
		return figura instanceof Circulo;
	}

	@Override
	public boolean validar() {
		Circulo cir = (Circulo)figura;
		return cir.getRadio()<=0;
	
	}

	@Override
	public String getError() {
		// hacer un downcast
		return "El radio debe ser mayor que 0 (cero)";
	}

}
