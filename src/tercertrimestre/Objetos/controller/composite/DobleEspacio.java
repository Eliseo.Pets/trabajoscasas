package tercertrimestre.controller.composite;

public class DobleEspacio extends ValidatorComposite {

	public DobleEspacio() {	}

	@Override
	public boolean isMe() {		
		return true;
	}

	@Override
	public boolean validar() {		
		return figura.getNombre().contains("  ");
	}

	@Override
	public String getError() {		
		return "El nombre no puede tener dos espacios";
	}

}
