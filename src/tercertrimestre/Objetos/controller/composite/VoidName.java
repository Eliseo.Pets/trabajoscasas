package tercertrimestre.controller.composite;

public class VoidName extends ValidatorComposite {

	public VoidName() {	}

	@Override
	public boolean isMe() {		
		return true;
	}

	@Override
	public boolean validar() {
		return figura.getNombre().isEmpty();
	}

	@Override
	public String getError() {
		return "El nombre no puede estar vacio";
	}

}
