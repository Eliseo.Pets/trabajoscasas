package tercertrimestre.controller.composite;

import tercertrimestre.modelo.Rectangulo;

public class RecAlturaNegativa extends ValidatorComposite {

	public RecAlturaNegativa() {
	
	}

	@Override
	public boolean isMe() {
		return figura instanceof Rectangulo;
	}

	@Override
	public boolean validar() {
		Rectangulo rec = (Rectangulo)figura;
		return rec.getAltura()<=0;
	
	}

	@Override
	public String getError() {
		// hacer un downcast
		return "La altura debe ser mayor que 0 (cero)";
	}

}
